package com.dev4abyss.yourband;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YourbandApplication {

	public static void main(String[] args) {
		SpringApplication.run(YourbandApplication.class, args);
	}

}
