package com.dev4abyss.yourband.dto;

import com.dev4abyss.yourband.entity.enums.CategoriaEnum;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MusicaDTO implements Serializable {
    private Long id;
    private String nome;
    private String urlCifra;
    private String urlVideo;
    private String urlAudio;
    private CategoriaEnum categoria;
    private String artista;
    private String letra;
    private UsuarioDTO usuario;

}