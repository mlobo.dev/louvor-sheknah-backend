package com.dev4abyss.yourband.controller;

import com.dev4abyss.yourband.dto.LogDTO;
import com.dev4abyss.yourband.mapper.LogMapper;
import com.dev4abyss.yourband.services.LogService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/logs")
@Tag(name = "Atividades",description = "Logs das últimas atividades realizadas")
public class LogController {


    private final LogService service;
    private final LogMapper mapper;

    @GetMapping
    public ResponseEntity<List<LogDTO>> listarTudo() {
        return ResponseEntity.ok(mapper.toDto(service.listarTudo()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<LogDTO> buscarPorId(@PathVariable("id") Long id) {
        return ResponseEntity.ok(mapper.toDto(service.buscarPorId(id)));
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletar(@PathVariable("id") Long id) {
        service.deletarLog(id);
        return ResponseEntity.noContent().build();
    }


}
