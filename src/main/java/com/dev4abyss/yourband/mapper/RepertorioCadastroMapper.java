package com.dev4abyss.yourband.mapper;


import com.dev4abyss.yourband.dto.RepertorioCadastroDTO;
import com.dev4abyss.yourband.entity.Repertorio;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RepertorioCadastroMapper extends BaseMapper<Repertorio, RepertorioCadastroDTO> {

}
