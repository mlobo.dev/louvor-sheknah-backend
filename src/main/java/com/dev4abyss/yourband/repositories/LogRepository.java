package com.dev4abyss.yourband.repositories;

import com.dev4abyss.yourband.entity.Log;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogRepository extends JpaRepository<Log, Long> {

}