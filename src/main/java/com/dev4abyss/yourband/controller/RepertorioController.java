package com.dev4abyss.yourband.controller;

import com.dev4abyss.yourband.dto.MusicaDTO;
import com.dev4abyss.yourband.dto.RepertorioCadastroDTO;
import com.dev4abyss.yourband.dto.RepertorioDTO;
import com.dev4abyss.yourband.entity.Repertorio;
import com.dev4abyss.yourband.mapper.MusicaMapper;
import com.dev4abyss.yourband.mapper.RepertorioMapper;
import com.dev4abyss.yourband.services.RepertorioService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;


@RestController
@RequestMapping("/repertorios")
@Tag(name = "Repertorios", description = "Recursos Sobre Repertórios")
@RequiredArgsConstructor
public class RepertorioController {


    private final RepertorioService repertorioService;
    private final RepertorioMapper repertorioMapper;
    private final MusicaMapper musicaMapper;


    @PostMapping
    @Operation(summary = "salva um novo recurso", description = "Cadastra um novo repertório na base")
    public ResponseEntity<RepertorioDTO> save(@Validated @RequestBody final RepertorioCadastroDTO dto) {
        return ResponseEntity.ok(repertorioMapper.toDto(repertorioService.salvar(dto)));
    }

    @GetMapping
    @Operation(summary = "Lista todos os Repertorios")
    public ResponseEntity<List<RepertorioDTO>> list() {
        List<Repertorio> list = repertorioService.listarTudo();
        return ResponseEntity.ok(repertorioMapper.toDto(list));
    }

    @GetMapping("/{id}")
    @Operation(summary = "Busca um departamento pelo id")
    public ResponseEntity<RepertorioDTO> buscarPeloId(@PathVariable("id") Long id) {
        return ResponseEntity.ok(repertorioMapper.toDto(repertorioService.buscarPeloId(id)));
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Exclui um departamento pelo id.")
    public ResponseEntity deleterCard(@PathVariable("id") Long id) {
        repertorioService.deletar(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping
    @Operation(summary = "Editar departamentos")
    public ResponseEntity<RepertorioDTO> editar(@Validated @RequestBody final RepertorioDTO departamentoDTO) {
        return ResponseEntity.ok(repertorioMapper.toDto(repertorioService.editar(departamentoDTO)));
    }

    @PostMapping("/{idRepertorio}/add-itens")
    @Operation(summary = "Adiciona musica através do id do departamento, e de uma lista de musica passada no corpo da requisição.")
    public ResponseEntity<RepertorioDTO> adicionarMusicas(@PathVariable("idRepertorio") Long idRepertorio, @RequestBody Set<MusicaDTO> itens) {
        return ResponseEntity.ok(
                repertorioMapper.toDto(repertorioService.adicionarItems(idRepertorio, musicaMapper.toEntity(itens)
                )));
    }

    @PostMapping("/{idRepertorio}/remover-funcionarios")
    @Operation(summary = "Remove musica através do id do departamento, e de uma lista de musica passada no corpo da requisição.")
    public ResponseEntity<RepertorioDTO> removerMusicas(@PathVariable("idRepertorio") Long idRepertorio, @RequestBody Set<MusicaDTO> itens) {
        return ResponseEntity.ok(
                repertorioMapper.toDto(repertorioService.removerItems(idRepertorio, musicaMapper.toEntity(itens)))
        );
    }


}
