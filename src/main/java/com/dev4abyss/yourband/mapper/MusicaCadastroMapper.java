package com.dev4abyss.yourband.mapper;


import com.dev4abyss.yourband.dto.MusicaCadastroDTO;
import com.dev4abyss.yourband.entity.Musica;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MusicaCadastroMapper extends BaseMapper<Musica, MusicaCadastroDTO> {

}
